#include<iostream>
using namespace std;

int tower[3][8] = {0};
int in_use[3] = {0};
const int DISKS = 8;

bool move(int count, int from, int to) {
    if(in_use[from]<count) return false;
    if(!count) return false;
    if(count == 1) {
        cout<<"Move disk "<<tower[from][in_use[from]-1]<<" from tower "<<from+1<<" to tower "<<to+1<<endl;
        tower[to][in_use[to]] = tower[from][in_use[from]-1];
        tower[from][in_use[from]-1] = 0;
        in_use[from]--;
        in_use[to]++;
        return true;
    }
    else {
        return move(count-1,from,3-from-to) &&
        move(1,from,to) &&
        move(count-1,3-from-to,to);
    }
}

int main() {
    for(int i=0;i<DISKS;i++)
        tower[0][i]=DISKS-(i);
    in_use[0]=DISKS;
    move(DISKS,0,1);
    return 0;
}
